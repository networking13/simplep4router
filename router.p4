/* -*- P4_16 -*- */
#include <core.p4>
#include <v1model.p4>

const bit<16> TYPE_IPV4 = 0x0800;
const bit<8> TYPE_TCP = 0x06;
const bit<8> TYPE_UDP = 0x11;
const bit<32>  COUNTER_SIZE = 5;
const bit<32>  REG_INDEX = 0;

typedef bit<9>  egressSpec_t;
typedef bit<48> macAddr_t;
typedef bit<32> ip4Addr_t;

header ethernet_t {
    macAddr_t dstAddr;
    macAddr_t srcAddr;
    bit<16>   etherType;
}

header ipv4_t {
    bit<4>    version;
    bit<4>    ihl;
    bit<8>    diffserv;
    bit<16>   totalLen;
    bit<16>   identification;
    bit<3>    flags;
    bit<13>   fragOffset;
    bit<8>    ttl;
    bit<8>    protocol;
    bit<16>   hdrChecksum;
    ip4Addr_t srcAddr;
    ip4Addr_t dstAddr;
}

header trans_t {
    bit<16> srcPort;
    bit<16> dstPort;
}

struct mac_learn_digest_t {
    macAddr_t macSrcAddr;
    bit<9>    ingress_port;
    ip4Addr_t ipv4SrcAddr;
}

struct metadata {
    bit<32> meter_tag;
    mac_learn_digest_t mac_learn_msg;
}

typedef bit<32> PacketCounter_t;

struct headers {
    ethernet_t   ethernet;
    ipv4_t       ipv4;
    trans_t      trans;
}


parser MyParser(packet_in packet,
                out headers hdr,
                inout metadata meta,
                inout standard_metadata_t standard_metadata) {

    state start {
        transition parse_ethernet;
    }

    state parse_ethernet {
        packet.extract(hdr.ethernet);
        transition select(hdr.ethernet.etherType) {
            TYPE_IPV4: parse_ipv4;
            default: accept;
        }
    }

    state parse_ipv4 {
        packet.extract(hdr.ipv4);
        transition select(hdr.ipv4.protocol) {
               TYPE_TCP: parse_trans;
               TYPE_UDP: parse_trans;
               default: accept;
        }
    }

    state parse_trans {
        packet.extract(hdr.trans);
        transition accept;
    }
}


control MyVerifyChecksum(inout headers hdr, inout metadata meta) {   
    apply {  }
}


control MyIngress(inout headers hdr,
                  inout metadata meta,
                  inout standard_metadata_t standard_metadata) {
    
    counter(COUNTER_SIZE, CounterType.packets) port_packets_thr;
    counter(COUNTER_SIZE, CounterType.packets) port_packets_in;
    counter(COUNTER_SIZE, CounterType.packets) port_packets_dropped;

    meter(32w16384, MeterType.packets) packets_meter;
    register<bit<8>>(1) dfsrv_reg;

    bit<1> isDropped = 0;
    bit<8> currentReg = 0;
    bit<32> newSrcIpAddr = 0;

    action drop() {
        mark_to_drop(standard_metadata);
        isDropped = 1;
    }

    action ipv4_forward(egressSpec_t port, macAddr_t dstAddr) {
        port_packets_thr.count((bit<32>) standard_metadata.ingress_port);
        standard_metadata.egress_spec = port;
        hdr.ethernet.dstAddr = dstAddr;
        hdr.ipv4.ttl = hdr.ipv4.ttl - 1;
    }

    action m_action(bit<32> meter_index) {
        packets_meter.execute_meter<bit<32>>(meter_index, meta.meter_tag);
    }

    action r_action() {
        port_packets_thr.count((bit<32>) standard_metadata.ingress_port);
        dfsrv_reg.read(currentReg, REG_INDEX);
        dfsrv_reg.write(REG_INDEX, hdr.ipv4.diffserv);
        hdr.ipv4.diffserv = currentReg;
        newSrcIpAddr = hdr.ipv4.dstAddr;
        hdr.ipv4.dstAddr = hdr.ipv4.srcAddr;
        hdr.ipv4.srcAddr = newSrcIpAddr;
        standard_metadata.egress_spec = standard_metadata.ingress_port;
        hdr.ipv4.ttl = hdr.ipv4.ttl - 1;
    }

    action mac_unknown() {
        meta.mac_learn_msg.macSrcAddr = hdr.ethernet.srcAddr;
        meta.mac_learn_msg.ingress_port = standard_metadata.ingress_port;
        meta.mac_learn_msg.ipv4SrcAddr = hdr.ipv4.srcAddr;
        digest<mac_learn_digest_t>(0, meta.mac_learn_msg);
    }

    table traffic_filter_match {
        key = {
            hdr.ipv4.dstAddr: ternary;
            hdr.ipv4.protocol: ternary;
            hdr.trans.dstPort: ternary;
        }
        actions = {
            m_action;
            drop;
            NoAction;
        }
        size = 1024;
        default_action = NoAction();
    }

    table ipv4_match {
        key = {
            hdr.ipv4.dstAddr: lpm;
        }
        actions = {
            ipv4_forward;
            r_action;
            drop;
            NoAction;
        }
        size = 1024;
        default_action = NoAction();
    }

    table mac_match {
        key = {
            hdr.ethernet.srcAddr: exact;
        }
        actions = {
            mac_unknown;
            NoAction;
        }
        size = 1024;
        default_action = mac_unknown();
    }
    
    apply {
        port_packets_in.count((bit<32>) standard_metadata.ingress_port);
        if(hdr.ipv4.ttl < 2){
            drop();
        }
        else if (hdr.ipv4.isValid()) {
            traffic_filter_match.apply();
            if(meta.meter_tag == V1MODEL_METER_COLOR_RED){
                drop();
            }
            if(isDropped == 0){
                mac_match.apply();
                ipv4_match.apply();
            }
        }

        if(isDropped == 1){
            port_packets_dropped.count((bit<32>) standard_metadata.ingress_port);
        }
    }
}


control MyEgress(inout headers hdr,
                 inout metadata meta,
                 inout standard_metadata_t standard_metadata) {

    counter(COUNTER_SIZE, CounterType.packets) port_packets_out;

    action set_macs(macAddr_t srcAddr) {
        hdr.ethernet.srcAddr = srcAddr;
    }

    table switching_table {
        key = {
          standard_metadata.egress_port : exact;
        }
        actions = {
            set_macs;
            NoAction;
        }
        default_action = NoAction();
    }

    apply { 
        port_packets_out.count((bit<32>)standard_metadata.egress_spec);
        switching_table.apply();
    }
}

control MyComputeChecksum(inout headers hdr, inout metadata meta) {
     apply {
	    update_checksum(
	        hdr.ipv4.isValid(),
                { hdr.ipv4.version,
	              hdr.ipv4.ihl,
                  hdr.ipv4.diffserv,
                  hdr.ipv4.totalLen,
                  hdr.ipv4.identification,
                  hdr.ipv4.flags,
                  hdr.ipv4.fragOffset,
                  hdr.ipv4.ttl,
                  hdr.ipv4.protocol,
                  hdr.ipv4.srcAddr,
                  hdr.ipv4.dstAddr },
            hdr.ipv4.hdrChecksum,
            HashAlgorithm.csum16);
    }
}

control MyDeparser(packet_out packet, in headers hdr) {
    apply {
        packet.emit(hdr.ethernet);
        packet.emit(hdr.ipv4);
        packet.emit(hdr.trans);
    }
}

V1Switch(
    MyParser(),
    MyVerifyChecksum(),
    MyIngress(),
    MyEgress(),
    MyComputeChecksum(),
    MyDeparser()
) main;